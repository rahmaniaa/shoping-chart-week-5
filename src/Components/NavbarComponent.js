import { Link } from 'react-router-dom';

const NavbarComponent = () => {
    return (
        <>
            <nav className='navbar navbar-light bg-body sticky-top shadow-sm'>
                <div className='container-lg'>
                    <Link to='/' className='navbar-brand'>
                        Sopiii
                    </Link>
                </div>
            </nav>
        </>
    );
};

export default NavbarComponent;
import axios from 'axios';

export const getListItem = () => {
    return async (dispatch) => {
        try {
            const result = await axios(
                'https://my-json-server.typicode.com/irhamhqm/placeholder-shops/items'
            );
            dispatch({ type: 'SET_LIST_ITEM', payload: result.data });
        } catch (err) {
            console.log(err);
        }
    };
};

export default addCart = (data) => {
    return {
        type: 'SET_CART',
        payload: data,
    };
};

export const adjustQty = (data) => {
    return {
        type: 'ADJUST_QTY',
        payload: data,
    };
};

export const adjustStok = (data) => {
    return {
        type: 'SET_STOK',
        payload: data,
    };
};
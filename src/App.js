import './App.css';
import NavbarComponent from './Components/NavbarComponent';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import ProductComponent from './Components/ProductComponent';
import CheckoutComponent from './Components/CheckoutComponent';

function App() {
  return (
    <>
    <Router className='App'>
      <NavbarComponent />
      <Routes>
        <Route path='/' element={<ProductComponent />} />
        <Route path='/checkout' element={<CheckoutComponent />} />
      </Routes>
    </Router>
    </>
  );
}

export default App;
